import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class GroupProvider {
  
  private response;
  private type;

  constructor(public http: Http) {}

  public call(type: string): Promise<any> {
    if (!this.response || this.type != type) {
      return new Promise(resolve => {
        this.http.get("https://roosters.deltion.nl/api/" + type)
          .subscribe(data => {
            this.response = data.json();
            this.response["data"] = [];
            this.type = type;

            if (type == "group" || type == "room") {
              for (var gr in data.json().data) {
                var element = data.json().data[gr];
                this.response.data.push(element);
              }
            } else if (type == "teacher") {
              for (var t in data.json().data) {
                this.response.data.push(t);
              }
            }

            resolve(data);
          })
      });
    } else {
      return new Promise(resolve => {
        resolve(this.response);
      });
    }
  }

  public getResponse() {
    return this.response;
  }

  public getType() {
    return this.type;
  }

}
