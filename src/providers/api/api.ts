import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Network } from '@ionic-native/network';

@Injectable()
export class ApiProvider {

  public response;
  public online;

  constructor(public http: Http, private network: Network) {}

  public call(group: string, date: Date, type: string, save = false): Promise<any> {
    if (group && date && type) {
      var fdate = [
        date.getFullYear().toString(),
        ((date.getMonth() + 1).toString().length == 1) ? "0" + (date.getMonth() + 1).toString() : (date.getMonth() + 1).toString(),
        (date.getDate().toString().length == 1) ? "0" + date.getDate().toString() : date.getDate().toString()
      ].join("");

      var ldate = [
        date.getFullYear().toString(),
        ((date.getMonth() + 1).toString().length == 1) ? "0" + (date.getMonth() + 1).toString() : (date.getMonth() + 1).toString(),
        ((date.getDate() + 1).toString().length == 1) ? "0" + (date.getDate() + 1).toString() : (date.getDate() + 1).toString()
      ].join("");

      return new Promise((resolve, reject) => {
        this.http.get(["https://roosters.deltion.nl/api/roster?", type, "=", group, "&start=", fdate, "&end=", ldate].join(""))
          .subscribe(data => {
            if (data.json().message == null) {
              this.response = data.json();
              this.online = true;
              
              if (save) {
                localStorage.setItem("timetable", JSON.stringify(this.response));
              }
              
              resolve(data);
            } else {
              this.response = JSON.parse(localStorage.getItem("timetable"));
              this.online = false;
              reject(["err"]);
            }
          }, error => {
            this.response = JSON.parse(localStorage.getItem("timetable"));
            this.online = false;
            reject(error);
          });
      });
    }
  }

  public checkOnline() {
    this.network.onDisconnect().subscribe(() => {
      this.online = false;
    });
   
    this.network.onConnect().subscribe(() => {
      this.online = true;
    });
  }

  public getResponse() {
    return this.response;
  }
}
