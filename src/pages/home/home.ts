import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { GroupPage } from '../group/group';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public time;
  public timetable;
  public date: Date;
  public loading: Boolean = false;

  constructor(public navCtrl: NavController, private apiProvider: ApiProvider) {
    this.timer();
    this.date = new Date();

    this.apiProvider.checkOnline();
    
    if (this.date.getDay() == 0) {
      this.date.setDate(this.date.getDate() + 1);
    } else if (this.date.getDay() == 6) {
      this.date.setDate(this.date.getDate() + 2);
    }

    this.call(localStorage.getItem("group"), this.date, true);
  }

  public call(group: string, date: Date, save = false) {
    this.loading = true;
    this.timetable = null;
    this.apiProvider.call(group, date, localStorage.getItem("type"), save).then(resolve => {
      this.timetable = this.apiProvider.getResponse().data[0];
      this.loading = false;
    }).catch(err => {
      this.timetable = this.apiProvider.getResponse().data[0];
      this.date = new Date();
      this.loading = false;
    });
  }

  public timer() {
    this.countTime();
    var home = this;
    setInterval(function() {
      home.countTime();
    }, 100);
  }

  private countTime() {
    var date = new Date();
    var hours = date.getHours().toString();
    var minutes = date.getMinutes().toString();
    var seconds = date.getSeconds().toString();
    
    if (hours.toString().length == 1) {
      hours = "0" + hours;
    }
    
    if (minutes.toString().length == 1) {
      minutes = "0" + minutes;
    }

    if (seconds.toString().length == 1) {
      seconds = "0" + seconds;
    }

    var time = [hours, minutes, seconds].join(":");
    this.time = time;
  }

  public next() {
    var tempDay = this.date.getDay() + 1;
    
    if (tempDay == 6) {
      this.date.setDate(this.date.getDate() + 3);
    } else if (tempDay == 0) {
      this.date.setDate(this.date.getDate() + 2);
    } else {
      this.date.setDate(this.date.getDate() + 1);
    }

    this.call(localStorage.getItem("group"), this.date);
  }

  public back() {
    var tempDay = this.date.getDay() - 1;
    
    if (tempDay == 0) {
      this.date.setDate(this.date.getDate() - 3);
    } else if (tempDay == 6) {
      this.date.setDate(this.date.getDate() - 2);
    } else {
      this.date.setDate(this.date.getDate() - 1);
    }

    this.call(localStorage.getItem("group"), this.date);
  }

  public today() {
    this.date = new Date();
    
    if (this.date.getDay() == 0) {
      this.date.setDate(this.date.getDate() + 1);
    } else if (this.date.getDay() == 6) {
      this.date.setDate(this.date.getDate() + 2);
    }

    this.call(localStorage.getItem("group"), this.date);
  }

  public changeGroup() {
    this.navCtrl.push(GroupPage);
  }

}