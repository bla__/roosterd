import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GroupProvider } from '../../providers/group/group';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-group',
  templateUrl: 'group.html',
})
export class GroupPage {

  public groups;
  public loading;
  public searchValue;

  constructor(public navCtrl: NavController, public navParams: NavParams, public groupProvider: GroupProvider) {}
  
  public call(type: string) {
    this.loading = true;
    this.groupProvider.call(type).then(resolve => {
      this.groups = this.groupProvider.getResponse().data;
      this.loading = false;
    });
  }

  public search() {
    var data = this.groupProvider.getResponse().data;
    var groups = [];

    for (var key in data) {
      var element = data[key];
      
      if (element.toLowerCase().startsWith(this.searchValue.toLowerCase())) {
        groups.push(element);
      }
    }

    this.groups = groups;
  }

  public changeGroup(group) {
    localStorage.setItem("group", group);
    localStorage.setItem("type", this.groupProvider.getType());
    this.navCtrl.setRoot(HomePage);
  }

}
